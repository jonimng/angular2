// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  url:'http://localhost/angular/slim/',
  firebase :{
    apiKey: "AIzaSyCM4_reg25TtXQ5hPpOF1uFu5PBt4BdgnY",
    authDomain: "joniboy052.firebaseapp.com",
    databaseURL: "https://joniboy052.firebaseio.com",
    projectId: "joniboy052",
    storageBucket: "joniboy052.appspot.com",
    messagingSenderId: "75897014014"
  }
};
