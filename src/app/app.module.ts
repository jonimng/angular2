import { RouterModule } from '@angular/router';
import { MessagesService } from './messages/messages.service';
import { UsersService } from './users/users.service';
import { HttpModule} from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';



import { AppComponent } from './app.component';
import { MessagesComponent } from './messages/messages.component';
import { MessagesFormComponent } from './messages/messages-form/messages-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersComponent } from './users/users.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { MessageComponent } from './messages/message/message.component';
import { LoginComponent } from './login/login.component';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { environment } from './../environments/environment';
import { MessagesfComponent } from './messagesf/messagesf.component';
import { MessageFormComponent } from './messages/message-form/message-form.component';


@NgModule({
  declarations: [ 
    AppComponent,
    MessagesComponent,
    MessagesFormComponent,
    UsersComponent,
    NavigationComponent,
    NotFoundComponent,
    MessageComponent,
    LoginComponent,
    MessagesfComponent,
    MessageFormComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase), 
    AngularFireDatabaseModule,
    RouterModule.forRoot([
      {path:'',component:MessagesComponent},
      {path:'login',component:LoginComponent},
      {path:'users',component:UsersComponent},
      {path:'message/:id',component:MessageComponent},
      {path:'messagesf',component:MessagesfComponent},
      {pathMatch:'full',path:'message-form/:id',component:MessageFormComponent},
      {path:'**',component:NotFoundComponent}
      
      

    ])

  ],
  providers: [
    MessagesService,
    UsersService  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
