import { Injectable } from '@angular/core';
import { Http } from '@angular/http';

@Injectable()
export class UsersService {

  http:Http;

  getUsers(){
  
    //return  ['Message1','Message2','Message3'];
    //get messegas from the SLIM rest API (dont say DB)
    return  this.http.get('http://localhost/angular/slim/users');

  }
  
  constructor(http:Http) { 
    this.http = http;
  }

}

